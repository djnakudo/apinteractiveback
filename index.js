const express = require('express')
const bodyParser = require('body-parser')
const allowCors = require('./config/cors');
require('dotenv').config();
const { Task } = require('./config/sequelize')


const app = express()
app.use(bodyParser.json())
app.use(allowCors)
// API ENDPOINTS

//API PAGINADA COM LIMITE DE 10 TASKS POR VEZ DE RETORNO
app.get('/tasks', (req, res) => {
    let limit = 10;   
    let offset = 0;
    Task.findAndCountAll()
    .then((data) => {
      let page = req.query.page || 1;     
      let pages = Math.ceil(data.count / limit);
          offset = limit * (page - 1);
      Task.findAll({
        attributes: ['id', 'content'],
        limit: limit,
        offset: offset,
        $sort: { id: -1 }
      })
      .then((tasks) => {
        res.status(200).json({'result': tasks, 'count': data.count, 'pages': pages});
      });
    })
    .catch(function (error) {
          res.status(500).send('Internal Server Error');
      });
  });


  //CRIAR nova task
  app.post('/tasks', (req, res) => {
      console.log(req.body)
    Task.create(req.body)
        .then(task => res.json(task))
        .catch(error=>res.json({
            error
        }))
})

//DELETAR task
app.delete('/tasks/:id',(req,res)=>{
    const {id} = req.params;
    Task.destroy({where :{
        id
    }})
    .then(msg=>res.status(201).json({
        msg:'Task was deleted'
    }))
    .catch(error=>res.json({
        error
    }))
})

//EDITAR task
app.put('/tasks/:id',(req,res)=>{
    const {id} = req.params;
    const {content} = req.body;
    Task.update({content},{where :{
        id
    }})
    .then(msg=>res.status(201).json({
       id,content
    }))
    .catch(error=>res.json({
        error
    }))
})

const port = process.env.SERVERPORT || 3000
app.listen(port, () => {
    console.log(`Running on http://localhost:${port}`)
})




