const Sequelize = require('sequelize')
const TaskModel = require('../models/task')
console.log(process.env.USERDB)
const sequelize = new Sequelize(process.env.DATABASE, process.env.USERDB, process.env.PASSWORDB, {
  host: process.env.HOST,
  dialect: 'mysql',
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})

const Task = TaskModel(sequelize, Sequelize)


sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created!`)
  })

module.exports = {
    Task
}