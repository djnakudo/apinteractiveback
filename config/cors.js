
//CASO O BACKEND E FRONTEND SEJAM EM SERVERS DIFERENTES, NAO LIMITAR ACESSO DO CORS DAS REQUESTS
module.exports = (req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
  	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  	next();
}
